import app from './app.js';
const PORT = process.env.PORT || 4000;
import './database.js';

app.listen(PORT, function(){ 
    console.log('Server: ' + PORT);
});