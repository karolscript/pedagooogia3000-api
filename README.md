Criteria acceptance

- Save user data:
Given a user with his name and valid email
when the user sends its data to the system
then the system saves the user data.

- Save user data: validate user data:
Given a user data with its name and invalid email
when the user sends its data to the system
then the system must return error validation message as
"The email must have the format 'user@mail.com'"