import express from 'express';

const server = express();

const port = process.env.port || 5000;

//const port = 5000;

// respond with "hello world" when a GET request is made to the homepage
server.get('/', (req, res) => {
    console.log('Listening in port ${port}')
    res.send('hello world');
    
  });
  
  server.listen(2222);

mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
.then(() => {
  console.log('MongoDB Connected…')
})
.catch(err => console.log(err))
