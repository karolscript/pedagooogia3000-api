import  mongoose  from 'mongoose';

//const TEST_URI = process.env.TEST_DB;
const TEST_URI = 'mongodb://localhost/pedagogia3000_test';

//So we wont use deprecated methods.
mongoose.set('useFindAndModify', false);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useNewUrlParser', true);

/**
 * Verifies DB Connection.
 */
before (done => {
    mongoose.connect(TEST_URI);
    mongoose.connection.once(
        "open", () => {
            console.log("Connected to DB_TEST");
            done();
        })
        .on ("error", err => {
            console.warn("Warning", err);

        });
});