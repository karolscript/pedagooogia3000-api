/**
 * Routes to User Service.
 */

import usersCtrl from '../controllers/users.js';

export default (app) => {
    app.get('/api/greeting', usersCtrl.greeting);
    app.get('/api/users', usersCtrl.index);
    app.get('/api/user/:id', usersCtrl.user);
    app.post('/api/users', usersCtrl.store);
    app.put('/api/users/:id', usersCtrl.update);
    app.delete('/api/users/:id', usersCtrl.destroy);
    app.patch('/api/users/:id', usersCtrl.partialUpdate);
};