import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const UserSchema = new Schema (
    {
        name: {
            type: String,
            required: 'The user name is required'
        },
        lastName: {
            type: String,
            required: 'The lastname is required'
        },
        email: {
            type: String,
            required: 'Te email is required'
        },
        isAvailable: {
            type: Boolean,
            default: false
        }
    },
    {
        timestamps: true
    }
);

const User = mongoose.model('user', UserSchema);

export default User;