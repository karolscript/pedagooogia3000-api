import express from 'express'
const app = express();

import bodyParser from 'body-parser';

import userRoutes from './routes/users.js';

/**
 * Once the petition is received it
 * will be parsed into a Json object.
 */
app.use(bodyParser.json());

/**
 * Calling user services.
 */
userRoutes(app);


app.use((err, req, res, next) => {
    res.status(400).send({
        errorMessage: err.message,
        errorName: err.name,
        err: err
    });
});


export default app;