import User from '../models/user.js';

export default {
    greeting(req, res) {
        return res.send({greeting: "Hi Users"});           
     },
    index(req, res) {
        User.find({}, (err, users) => {
            res.status(200).send({data: users});
        },err => {
            console.log(err);
        });             
    },
    
    user(req, res, next) {
        const id = req.params.id;
        
       const getUserCompleteInfo = async () => {
            try {
                User.findOne(id);
                return res.status(200).send({ data: user });
            }
            catch(e) { return e }
        }
        User.find({}, (err, users) => {
            res.status(200).send({data: users});
        },err => {
            console.log(err);
        });           
    },
    /** 
     * @param {* Object to store. } req 
     * @param {* Database response.} res 
     * @param {* Go to next middleware. } next 
     */
    store(req, res, next) {
        const userProps = req.body;
        User.create( userProps )
        .then( user => res.status(201).send({ data: user }))
        .catch(next);
    },
    update(req, res, next) {
        const userProps = req.body;
        User.findByIdAndUpdate(
            { _id: req.params.id },
            { $set: userProps },
            { new: true })
        .then(user => {
            if (!user) {
                return res.status(404).send('User not found');
            }
            return res.status(200).send({ data: user });
        }).catch(next);
    },
    destroy(req, res, next) {
        const userId = req.params.id;
        User.findByIdAndRemove(userId).then(user => {
            if (!user) {
                return res.status(404).send('User not found');
            }
            return res.status(200).send();
        }).catch(next);
        
    },
    partialUpdate(req, res) {
        return res.status(201).send(req.body);
    }
};