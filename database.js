import dotenv from 'dotenv';
import  mongoose  from 'mongoose';

dotenv.config({path:'./.env'});


const MONGO_URI = process.env.DEV_DB;

mongoose.Promise = global.Promise;

//So we wont use deprecated methods.
mongoose.set('useFindAndModify', false);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useNewUrlParser', true);

mongoose.connect(MONGO_URI).then(() => {
    console.log('Woooojooo we are connected');
},
    err => {
        console.log('Something ugly happened');
        console.log(err.stack);
    }
);